from django.contrib import admin

# Register your models here.
from django.contrib import admin
from gamers.models import Gamer

class GamerAdmin(admin.ModelAdmin):
    fields = ('first_name', 'last_name', 'email', 'avatar', 'rating', 'wins', 'failures', 'is_active', 'updated', 'created',)
    list_display = ('first_name', 'last_name', 'email', 'avatar', 'rating', 'wins', 'failures', 'is_active', 'updated', 'created')
    search_fields = ('first_name', 'last_name', 'email', 'avatar')
    list_filter = ('rating', 'is_active',)
    ordering = ['last_name']

    def get_readonly_fields(self, request, obj=None):
        return ['created', 'updated',]

admin.site.register(Gamer, GamerAdmin)

# 'id', 'first_name', 'last_name', 'email', 'avatar', 'rating', 'wins', 'failures', 'is_active', 'updated', 'created'