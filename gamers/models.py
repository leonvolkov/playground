from django.db import models
from django.core.validators import EmailValidator

from main.enums import GAMER_RATING

class Gamer(models.Model):
    id = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email = models.CharField(max_length=50, blank=True, null=True, validators=[EmailValidator])
    avatar = models.ImageField(blank=True, null=True, upload_to='./static/upload/avatars')

    rating = models.PositiveIntegerField(choices=GAMER_RATING.choices, default=0)
    wins = models.PositiveIntegerField(default=0)
    failures = models.SmallIntegerField(default=0)

    is_active = models.BooleanField(default=False)
    updated = models.DateField(auto_now = True)
    created = models.DateField(auto_now_add = True)

    def __unicode__(self):
        return u'{}'.format(self.get_full_name())

    def __str__(self):
        return self.get_full_name()

    def get_full_name(self):
        '''
        Returns the first_name plus the last_name, with a space in between
        or email if there no names.
        '''
        if self.first_name or self.last_name:
            full_name = '{0} {1}'.format(self.first_name, self.last_name).strip()
        else:
            full_name = self.email
        return full_name

# 'id', 'first_name', 'last_name', 'email', 'avatar', 'rating', 'wins', 'failures', 'is_active', 'updated', 'created'