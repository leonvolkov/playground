"""
main app enums
"""

class GAMER_RATING(object):
    loser = 0
    experienced = 1
    advanced = 2
    master = 3
    guru = 4

    choices = (
        (loser, "Loser"),
        (experienced, "Experienced"),
        (advanced, "Advanced"),
        (master, "Master"),
        (guru, "Guru"),
    )
