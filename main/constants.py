"""
main app constants
"""

# Date format for render in UI
VIEW_MONTH_FORMAT = '%b %Y'
VIEW_DATE_FORMAT = '%b %d, %Y'
VIEW_DATETIME_FORMAT = VIEW_DATE_FORMAT + ' %H:%M:%S'

# Internal datetime format to use in python
ISO_DATE_FORMAT     = '%Y-%m-%d'
ISO_DATETIME_FORMAT = ISO_DATE_FORMAT + 'T%H:%M:%SZ'

# Date format for API
API_DATE_FORMAT = '%Y-%m-%d'
API_DATETIME_FORMAT = API_DATE_FORMAT + ' %H:%M:%S'

# String default
NONE_STRING = 'N/A'
