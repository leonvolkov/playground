from rest_framework.routers import DefaultRouter
from gamers.api.v1.views import GamersViewSet


router = DefaultRouter()
# 1st level
persons_router = router.register(r'persons', GamersViewSet, base_name='person')
